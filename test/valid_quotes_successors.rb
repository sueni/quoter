require 'minitest/autorun'
require_relative '../lib/valid_quotes_successors'

describe 'valid quotes successors hash' do
  it 'should be frozen' do
    assert ValidSuccessors::SEQUENCE.frozen?
  end
end

describe 'valid quotes successors hash' do
  it 'should always be the same object' do
    q1 = Quotes.new('""')
    q2 = Quotes.new('«»')
    q1.valid_quotes_successors.must_be_same_as q2.valid_quotes_successors
  end
end

describe 'valid quotes successors' do
  it 'should be equal' do
    {
      ?« => %w[» „],
      ?„ => %w[“ ‹],
      ?‹ => %w[› ‚],
      ?‚ => %w[‘],
      ?‘ => %w[‚ ›],
      ?› => %w[‹ “],
      ?“ => %w[„ »],
      ?» => %w[«],
    }.must_equal ValidSuccessors::SEQUENCE
  end
end
