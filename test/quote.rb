require 'minitest/autorun'
require_relative '../lib/quote'

describe Quote do
  before do
    @openers = Quote::OPENING_QUOTES
    @closers = Quote::CLOSING_QUOTES
  end

  it 'should ensure openers and closers are immutable' do
    assert @openers.frozen?
    assert @closers.frozen?
  end

  it 'should ensure the number of opening and closing quotes are equal' do
    @openers.size.must_equal @closers.size
  end

  it 'should ensure the number of valid quotes is even' do
    (@openers + @closers).size.even?
  end

  it 'should check openers and closers are not intersected' do
    all_quotes = @openers + @closers
    unique_quotes = all_quotes.uniq
    all_quotes.must_equal unique_quotes
  end

  it 'should raise IndexError on attempt to get quote by negative index' do
    err = proc { Quote.opener_of_lvl(-1) }.must_raise IndexError
    err.message.must_equal 'Requested quote by restricted index: -1'

    err = proc { Quote.closer_of_lvl(-1) }.must_raise IndexError
    err.message.must_equal 'Requested quote by restricted index: -1'
  end

  it 'should raise IndexError on attempt to get quote by exceeding index' do
    err = proc { Quote.opener_of_lvl(Quote::MAX_LVL + 1) }.must_raise IndexError
    err.message.must_equal 'Requested quote by restricted index: 4'

    err = proc { Quote.closer_of_lvl(Quote::MAX_LVL + 1) }.must_raise IndexError
    err.message.must_equal 'Requested quote by restricted index: 4'
  end

  it 'should return opener quote of lvl 0' do
    Quote.opener_of_lvl(0).must_equal '«'
  end

  it 'should return opener quote of lvl 3' do
    Quote.opener_of_lvl(3).must_equal '‚'
  end

  it 'should return closer quote of lvl 0' do
    Quote.closer_of_lvl(0).must_equal '»'
  end

  it 'should return closer quote of lvl 3' do
    Quote.closer_of_lvl(3).must_equal '‘'
  end

  it 'should return corresponding closer for an opener' do
    @openers.each_with_index do |opener, i|
      Quote.pair(opener).must_equal @closers[i]
    end
  end

  it 'should return corresponding opener for a closer' do
    @closers.each_with_index do |closer, i|
      Quote.pair(closer).must_equal @openers[i]
    end
  end

  it 'should check the type of last quote' do
    [:opener, :closer, nil].must_include Quote.last_quote_type

    Quote.opener_of_lvl(0)
    Quote.last_quote_type.must_equal :opener

    Quote.closer_of_lvl(0)
    Quote.last_quote_type.must_equal :closer
  end
end
