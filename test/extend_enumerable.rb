require 'minitest/autorun'
require_relative '../lib/extend_enumerable'
using RefineString

describe 'hiding tags' do
  it 'should process parts of line out of tags' do
    {
      ' <foo>foo<foo>foo ' => ' <foo>fOO<foo>fOO ',
    }
      .each do |sample, expectation|
      result = sample.chunk_on_tags.hide_tags { |chunk| chunk.gsub('oo', 'OO') }
      result.join.must_equal expectation
    end
  end
end

describe 'exposing quotes' do
  it 'should ensure quotes always have odd index' do
    [
      '«1 „2 ‹3 ‚4‘ 3›“»',
      ' «„я“-центричное действие»',
      '"1 "2 "3 "4" 3"""',
      'foo',
    ]
      .each do |sample|
      result = sample.chunk_on_quotes.expose_quotes do |quote, idx, _, _|
        assert idx.odd?
        quote
      end
      result.join.must_equal sample
    end
  end

  it 'should ensure quotes are really exposed' do
    {
      '«1 „2 ‹3 ‚4‘ 3›“»' => '*1 *2 *3 *4* 3***',
      ' «„я“-центричное действие»' => ' **я*-центричное действие*',
      '"1 "2 "3 "4" 3"""' => '*1 *2 *3 *4* 3***',
      'foo' => 'foo',
    }
      .each do |sample, expectation|
      result = sample.chunk_on_quotes.expose_quotes do |quote, idx, _, _|
        '*'
      end
      result.join.must_equal expectation
    end
  end
end
