require 'minitest/autorun'
require_relative '../lib/quotes'

describe 'finding quotes' do
  it 'should test found quotes are immutable' do
    assert Quotes.new('""').quotes.frozen?
  end

  it 'should ensure both unpaired and and paired quotes have found' do
    test_string = Quote::ALL_QUOTES.join
    test_string.size.must_equal Quotes.new(test_string).size
  end

    it 'should find no quotes' do
      string = '<a type="note" l:href="#n1">[1]</a>'
      Quotes.new(string).quotes.must_be_empty
    end

    it 'should find 8 quotes' do
      string = '«foo»<a type="note" l:href="#n1">[1]</a>‹bar›'
      Quotes.new(string).size.must_equal 8
    end

    it 'should find 4 quotes' do
      string = '<a type="note" l:href="#n1">[1]</a>‹bar›«foo»'
      Quotes.new(string).size.must_equal 4
    end
end

describe 'quotes even' do
  it 'should be even' do
    quotes = Quotes.new '""'
    assert quotes.even?
  end

  it 'should not be even' do
    quotes = Quotes.new '«"»'
    refute quotes.even?
  end
end

describe 'nesting order' do
  before do
    @o = Quote::OPENING_QUOTES
    @c = Quote::CLOSING_QUOTES
  end

  it 'ensures opening and closing quotes are well-ordered' do
    string_of_quotes = (@o + @c.reverse).join
    assert Quotes.new(string_of_quotes).nested?
  end

  it 'should always refute to pass if a unpaired quote occurs' do
    quotes = Quotes.new '""'
    refute quotes.nested?
  end

  it 'should pass test for nesting' do
    %w{
      «»
      «„“»
      «„‹›“»
      «„‹‚‘›“»
      «„‹›“»«„“»«»
      «„‹‚‘›“»«»«»
      «„“„“»
      «„‹‚‘‚‘›‹›“„“»
    }.each do |sample|
      assert Quotes.new(sample).nested?
    end
  end

  it 'should fail test for nesting' do
    %w{
      »«
      „“
      „«“»
      «‹›»
      „‹›“
      ‹›
      «‹„›“»
      «„‚‹‘›“»
      «„‹“›»«„“»«»
      «„‹‚›‘“»«»«»
      «„»«»»«»
      ««»
      «„“‹›„“»
    }.each do |sample|
      refute Quotes.new(sample).nested?
    end
  end

  it 'should pass test for nesting even without any quotes' do
    assert Quotes.new('foo').nested?
  end
end

describe 'arrange quotes' do
  it 'should arrange quotes by frequency' do
  original = ('«„‹‚‘›“»«»«»')
  expectation = {
    '«' => 3,
    '„' => 1,
    '‹' => 1,
    '‚' => 1,
    '‘' => 1,
    '›' => 1,
    '“' => 1,
    '»' => 3
  }
    Quotes.new(original).arrange.must_equal(expectation)
  end
end
