require 'minitest/autorun'
require_relative '../lib/refine_string'
require_relative '../lib/engine'
require_relative '../lib/options'

describe Engine do
  before do
    options = Parser.parse([])
    @engine = Engine.new(options)
  end

  it 'should fix not nested quotes' do
    string = '"foo"'
    @engine.fix(string).must_equal '«foo»'

    string = '‹foo" ‘foo"'
    @engine.fix(string).must_equal '«foo» «foo»'

    string = '<"foo"> "foo"\n"foo""foo"'
    @engine.fix(string).must_equal '<"foo"> «foo»\n«foo»«foo»'

    string = '< "Фрейд — это подделка<a l:href="#n1" type="note">[1]</a> века"'
    @engine.fix(string).must_equal '< «Фрейд — это подделка<a l:href="#n1" type="note">[1]</a> века»'

    string = %[ "1 \n 2"]
    @engine.fix(string).must_equal " «1 \n 2»"

    string = '<emphasis>«dual»</emphasis>'
    @engine.fix(string).must_equal '<emphasis>«dual»</emphasis>'

    string = 'на «я» и не-«я», мы'
    @engine.fix(string).must_equal 'на «я» и не-«я», мы'
  end

  it 'should fix two level nested quotes' do
    string = '"foo "bar""'
    @engine.fix(string).must_equal '«foo „bar“»'

    string = '"foo "bar" baz"'
    @engine.fix(string).must_equal '«foo „bar“ baz»'
  end

  it 'should fix whatever cases' do
    string = '"1 "2 "3 "4""""'
    @engine.fix(string).must_equal '«1 „2 ‹3 ‚4‘›“»'

    string = '"1 "2 "3 "4""""'
    @engine.fix(string).must_equal '«1 „2 ‹3 ‚4‘›“»'

    string = '""""4" 3" 2" 1"'
    @engine.fix(string).must_equal '«„‹‚4‘ 3› 2“ 1»'

    string = '«механизм сохранения „я“»'
    @engine.fix(string).must_equal '«механизм сохранения „я“»'

    string = '«„я“-центричное действие»'
    @engine.fix(string).must_equal '«„я“-центричное действие»'

    source = '<>«молекулы CH<sub>3</sub>COOC<sub>5</sub>H<sub>11</sub>„ '
    expect = '<>«молекулы CH<sub>3</sub>COOC<sub>5</sub>H<sub>11</sub>» '
    @engine.fix(source).must_equal expect

    source = '«Я не понимаю, ... Всё это майя [„иллюзия“ на санскрите]. А если ... неправда».'
    @engine.fix(source).must_equal source

    source = '«спросим: <emphasis>»А разве могло быть иначе?«</emphasis>»'
    expect = '«спросим: <emphasis>„А разве могло быть иначе?“</emphasis>»'
    @engine.fix(source).must_equal expect
  end
end
