require 'minitest/autorun'
require_relative '../lib/level'

describe Level do
  before do
    @lvl = Level.new(max_lvl: Quote::MAX_LVL)
  end

  it 'lvl should be -1 initially' do
    @lvl.get.must_equal(-1)
  end

  it 'should raise RangeError on down at initial state' do
    proc { @lvl.down }.must_raise RangeError
  end

  it 'should return 0 and lvl should become 0 after initial up' do
    @lvl.up.must_equal 0
    @lvl.get.must_equal 0
  end

  it 'should return 0 on down at lvl 0' do
    @lvl.set 0
    @lvl.down.must_equal 0
    @lvl.get.must_equal(-1)
  end

  it 'should be -1 on equal numbers of ups and downs' do
    4.times{ @lvl.up }
    4.times{ @lvl.down }

    4.times do
      @lvl.up
      @lvl.down
    end

    @lvl.get.must_equal(-1)
  end

  it 'should raise RangeError on up after max lvl' do
    @lvl.set 3
    proc { @lvl.up }.must_raise RangeError
  end
end
