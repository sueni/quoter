require 'minitest/autorun'
require_relative '../lib/refine_string'
using RefineString

describe RefineString do
  it 'should decide which string is xml' do
    assert '<bar'.is_xml?
    assert ' <bar'.is_xml?
    assert '	<bar'.is_xml?
    refute 'foo<bar'.is_xml?
  end

  it 'should remove tags from string' do
    'foo<bar>foo'.remove_tags.must_equal 'foofoo'
  end

  it 'should chunk string on tags and no-tags' do
    '<bar>foo'.chunk_on_tags.must_equal ['', '<bar>', 'foo']
    '<baz><bar>foo'.chunk_on_tags.must_equal ['', '<baz>', '', '<bar>', 'foo']

    '<baz>hello<bar>foo'.chunk_on_tags
      .must_equal ['', '<baz>', 'hello', '<bar>', 'foo']

  '< «Фрейд — это подделка<a l:href="#n1" type="note">[1]</a> века»' .chunk_on_tags
    .must_equal ['< «Фрейд — это подделка', '<a l:href="#n1" type="note">', '[1]', '</a>', ' века»']
  end

  it 'should chunk string on quotes and no-quotes' do
    '«1 „2 ‹3 ‚4‘ 3›“»'.chunk_on_quotes
      .must_equal [
        '',
        '«',
        '1 ',
        '„',
        '2 ',
        '‹',
        '3 ',
        '‚',
        '4',
        '‘',
       ' 3',
       '›',
       '',
       '“',
       '',
       '»',
    ]
  end
end
