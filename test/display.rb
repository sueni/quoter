require 'minitest/autorun'
require_relative '../lib/display'

describe Display do
  before do
    options = Parser.parse([])
    @display = Display.new(options)
  end

  it 'should print step symbols in one line' do
    n = 2
    proc { n.times { @display.step { '' } } }.must_output { (Display::STEP * n) }
  end

  it 'should evaluate the block' do
    @display.step { 'foo' }.must_equal 'foo'
  end

  it 'should print arranged quotes (sample 1)' do
    arranged_quotes = Quotes.new('«»"„').arrange

    expect = " « -> 1\n" \
      " » -> 1\n" \
      "\e[1m \" -> 1\e[0m\n" \
      "\e[1m „ -> 1\e[0m\n"

    proc { @display.show_stats(arranged_quotes) }.must_output expect
  end

  it 'should print arranged quotes (sample 2)' do
    arranged_quotes = Quotes.new('""«»„“‹›‚‘').arrange
    expect = " \" -> 2\n" \
      " « -> 1\n" \
      " » -> 1\n" \
      " „ -> 1\n" \
      " “ -> 1\n" \
      " ‹ -> 1\n" \
      " › -> 1\n" \
      " ‚ -> 1\n" \
      " ‘ -> 1\n" \

      proc { @display.show_stats(arranged_quotes) }.must_output expect
  end
end
