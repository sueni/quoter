#!/bin/sh

arrange() {
	[ -d "$2" ] || mkdir $2
	mv $1 $2
}

for i in *fb2; do
	`dirname $0`/../quoter.rb -o $i
	return_code=$?
	if [ $return_code -ne 0 ]; then
		arrange $i $return_code
	fi
done
