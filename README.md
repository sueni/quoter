# Quoter

Утилита для приведения кавычек к типографским стандартам.

Поддерживаются как обычный текст, так и xml-производные форматы.

Различаются четыре уровня вложенности кавычек:

- «первый уровень»
- „второй уровень“
- ‹третий уровень›
- ‚четвёртый уровень‘


```
Usage: quoter.rb [options] <input_file>
		-d, --debug                      Enable debugging mode
		-o, --overwrite                  Overwrite the source file
```
