class Level
  def initialize(max_lvl: lvl)
    @max_lvl = max_lvl
    @lvl = -1
  end

  def up
    alter(__method__) { @lvl += 1 }
  end

  def down
    alter(__method__) { @lvl -= 1 }
  end

  def get
    @lvl
  end

  def set(lvl)
    @lvl = lvl
  end

  private

    def alter(method)
      lvl_was = @lvl

      yield

      unless (-1..@max_lvl).cover? @lvl
        fail RangeError,
          "Quotes level is out of range: #@lvl"
      end

      case method
      when :up
        @lvl
      when :down
        lvl_was
      end
    end
end
