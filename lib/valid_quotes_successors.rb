module ValidSuccessors
  def self.build_valid_quotes_sequence(h={})
    builder(Quote::OPENING_QUOTES, Quote::CLOSING_QUOTES, :last,   1, h)
    builder(Quote::CLOSING_QUOTES, Quote::OPENING_QUOTES, :first, -1, h)
    h.freeze
  end

  def self.builder(for_quotes, with_quotes, edge, idx_shift, hash={})
    for_quotes.each_with_index do |q, idx|
      if q == for_quotes.public_send(edge)
        hash[q] = [with_quotes.public_send(edge)]
      else
        hash[q] = [with_quotes[idx], for_quotes[idx+idx_shift]]
      end
    end
    hash
  end

  SEQUENCE ||= build_valid_quotes_sequence
end
