require 'optparse'

class Parser
  Options = Struct.new(:debug, :overwrite)

  def self.parse(options)
    args = Options.new(false, false)

    opt_parser = OptionParser.new do |opts|
      opts.banner = "Usage: #{File.basename($0)} [options] <input_file>"

      opts.on('-d', '--debug', 'Enable debugging mode') do |d|
        args.debug = d
      end

      opts.on('-o', '--overwrite', 'Overwrite the source file') do |o|
        args.overwrite = o
      end
    end

    opt_parser.parse!(options)
    return args
  rescue OptionParser::InvalidOption => e
    puts e, opt_parser.help
    exit 1
  end
end
