require 'forwardable'
require_relative 'valid_quotes_successors'

class Quotes
  using RefineString
  attr_reader :quotes

  extend ValidSuccessors
  extend Forwardable

  def_delegators '@quotes', :size
  def_delegator '@quotes.size', :even?, :even?

  def initialize(string)
    string = string.remove_tags if string.is_xml?
    @quotes = string.scan(/[#{Quote::ALL_QUOTES.join}]/).freeze
  end

  def arrange
    h = Hash.new(0)
    @quotes.each { |q| h[q] += 1 }
    h
  end

  def nested?
    return true if @quotes.empty?

    return false unless @quotes.first == Quote::OPENING_QUOTES.first and
      @quotes.last == Quote::CLOSING_QUOTES.first

    @quotes.each_with_index do |quote, idx|
      next_quote = @quotes[idx+1]
      if next_quote
        unless  valid_quotes_successors[quote].include? next_quote
          return false
        end
      end
    end
    true
  end

  def valid_quotes_successors
    ValidSuccessors::SEQUENCE
  end
end
