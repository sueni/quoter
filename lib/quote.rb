module Quote
  QuoteError = Class.new(StandardError)

  attr_reader :last_quote_type
  extend self

  ALL_QUOTES = [
    INVALID_QUOTES = %w{"”}.freeze,
    OPENING_QUOTES = %w{« „ ‹ ‚}.freeze,
    CLOSING_QUOTES = %w{» “ › ‘}.freeze,
  ]

  MAX_LVL = OPENING_QUOTES.size - 1

  def opener_of_lvl(lvl)
    verify_index(lvl)
    @last_quote_type = :opener
    OPENING_QUOTES[lvl]
  end

  def closer_of_lvl(lvl)
    verify_index(lvl)
    @last_quote_type = :closer
    CLOSING_QUOTES[lvl]
  end

  def pair(quote)
    ((idx = OPENING_QUOTES.index(quote)) && CLOSING_QUOTES[idx]) ||
      ((idx = CLOSING_QUOTES.index(quote)) && OPENING_QUOTES[idx])
  end

  private

    def verify_index(lvl)
      unless (0..MAX_LVL).cover? lvl
        fail IndexError,
          "Requested quote by restricted index: #{lvl}"
      end
    end
end
