module Enumerable
  def hide_tags
    map do |chunk|
      if chunk.chr == '<' and chunk[-1] == '>'
        chunk
      else
        yield chunk
      end
    end
  end

  def expose_quotes
    prev_chunk = ''

    map.with_index do |chunk, i|
      next_chunk = self[i+1]
      if i.odd?
        yield chunk, i, prev_chunk, next_chunk
      else
        prev_chunk = chunk
      end
    end
  end
end
