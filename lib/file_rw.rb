class FileRw
  def initialize(file, options)
    @file = file
    @options = options
  end

  def read
    @file.read
  rescue Errno::ENOENT
    abort("Ошибочный файл: #{@file.filename}")
  end

  def write(string, updated: true)
    if $stdin.tty?
      if updated
        File.write(output_filename, string)
      end
    else
      puts string
    end
  end

  private

    def output_filename
      if @options.overwrite
        @file.filename
      else
        File.basename(@file.filename, '.*') +
          '.q' +
          File.extname(@file.filename)
      end
    end
end
