class Engine
  using RefineString

  def initialize(options)
    @lvl = Level.new(max_lvl: Quote::MAX_LVL)
    @options = options
  end

  def fix(string)
    if string.is_xml?
      string.chunk_on_tags.hide_tags do |no_tag|
        no_tag.chunk_on_quotes.expose_quotes do
          |quote, idx, prev_chunk, next_chunk|
          pick_proper(quote, idx, prev_chunk, next_chunk)
        end
      end.join
    else
      string.chunk_on_quotes.expose_quotes do
        |quote, idx, prev_chunk, next_chunk|
        pick_proper(quote, idx, prev_chunk, next_chunk)
      end.join
    end
  end

  private

    def pick_proper(quote, idx, prev_chunk, next_chunk)
      show_debug_context(idx, quote, prev_chunk, next_chunk) if @options.debug

      if @lvl.get == -1
        Quote.opener_of_lvl @lvl.up
      elsif prev_chunk.empty?
        if /\p{Space}/.match? next_chunk
          Quote.closer_of_lvl @lvl.down
        else
          # keep the tendency of level increasing/decreasing
          case Quote.last_quote_type
          when :opener then Quote.opener_of_lvl @lvl.up
          when :closer then Quote.closer_of_lvl @lvl.down
          end
        end
      else
        if prev_chunk.end_with?(' ', ? , ?[, ?() or
            /<[\w-]+?>$/.match? prev_chunk
          Quote.opener_of_lvl @lvl.up
        else
          Quote.closer_of_lvl @lvl.down
        end
      end
    end

    def show_debug_context(*args)
      args[2] = args[2].inspect
      args[-1] = args[-1].inspect
      puts 'Idx: %-3s Quote: %s Prev_chunk: %-4.4s Next_chunk: %s' % args
    end
end
