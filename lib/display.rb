class Display
  STEP = '.'

  def initialize(options)
    @options = options
    @loud = loud?
  end

  def step
    print STEP if @loud
    yield
  end

  def no_need_to_worry
    puts '=' if @loud
    yield
  end

  def success
    puts green '+' if @loud
  end

  def fail
    puts red '-'
  end

  def dirty_hands(arranged_quotes)
    puts yellow '?'
    show_stats(arranged_quotes)
  end

  def show_stats(arranged_quotes)
    arranged_quotes.each do |q,n|
      template = ' %s -> %s' % [q,n]

      if pair = Quote.pair(q)
        puts arranged_quotes[pair] == n ? template : bold(template)
      else
        puts n.even? ? template : bold(template)
      end
    end
  end

  private

    def loud?
      !@options.debug and $stdin.tty?
    end

    def bold(string)
      "\e[1m#{string}\e[0m"
    end

    def red(string)
      "\e[31m#{string}\e[0m"
    end

    def green(string)
      "\e[32m#{string}\e[0m"
    end

    def yellow(string)
      "\e[33m#{string}\e[0m"
    end
end
