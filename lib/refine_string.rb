module RefineString
  refine String do
    def is_xml?
      self[/\S/] == '<'
    end

    def remove_tags
      split(/<[^>]+?>/).join
    end

    def chunk_on_tags
      split(/(<[^<>]+?>)/)
    end

    def chunk_on_quotes
      split(/([#{Quote::ALL_QUOTES.join}])/)
    end
  end
end
