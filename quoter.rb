#!/usr/bin/ruby
# frozen_string_literal: true
# ──────────────────────────────────────────────────────────────────────────────
# Takes only one file (as argument of from STDIN) and handles it accordingly
# ──────────────────────────────────────────────────────────────────────────────
%w{
  refine_string
  extend_enumerable
  quote
  quotes
  display
  level
  engine
  file_rw
  options
}.each do |m|
  require_relative File.join('lib', m)
end

options = Parser.parse ARGV
rw      = FileRw.new(ARGF, options)
display = Display.new options
string  = display.step{ rw.read }
quotes  = display.step{ Quotes.new string }

unless display.step{ quotes.even? }
  display.dirty_hands quotes.arrange
  exit 2
end

if display.step{ quotes.nested? }
  display.no_need_to_worry { rw.write(string, updated: false) }
else

  begin
    result = display.step{ Engine.new(options).fix(string) }
  rescue RangeError
    display.fail
    exit 3
  end

  quotes = display.step{ Quotes.new result }

  if display.step{ quotes.nested? }
    display.step{ rw.write(result) }
    display.success
  else
    display.fail
    exit 3
  end
end
